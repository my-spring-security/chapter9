create table users
(
    username varchar(50)  not null primary key,
    password varchar(500) not null,
    enabled  tinyint(1) not null
);

create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null
);

-- 这里用户的密码是使用SpringSecurity的加密类得到的结果
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('lh0811', '$2a$10$GU2QxycINcOKbunq5fxg1.OIaYQKxf8ajmn8nOh2YPIS674d.mVs.', 1);

INSERT INTO `my-springsecurity`.`authorities` (`username`, `authority`) VALUES ('lh0811', 'MYROLE_WORKER');
INSERT INTO `my-springsecurity`.`authorities` (`username`, `authority`) VALUES ('lh0811', 'user:add');