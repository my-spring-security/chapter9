package com.lh0811.security.chapter9.auth;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.io.IOException;
import java.util.stream.Collectors;

/**
 * 自定义认证filter，需要继承于AbstractAuthenticationProcessingFilter
 */
@Slf4j
public class MyUsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    // 定义默认的RequestMatcher
    private static final AntPathRequestMatcher DEFAULT_ANT_PATH_REQUEST_MATCHER = new AntPathRequestMatcher("/my_custom/login", "POST");

    // 无参构造方法，创建默认配置的Filter
    public MyUsernamePasswordAuthenticationFilter() {
        super(DEFAULT_ANT_PATH_REQUEST_MATCHER);
    }

    // 自定义RequestMatcher的构造方法
    public MyUsernamePasswordAuthenticationFilter(AntPathRequestMatcher matcher) {
        super(matcher);
    }

    // 提供attemptAuthentication方法，该方法的主要作用是从Request中获取到用户名密码并组成未认证的AuthenticationToken
    // 组成未认证的AuthenticationToken后,调用AuthenticationManager(实际为ProviderManager)去认证AuthenticationToken
    // 认证成功后，返回已认证的AuthenticationToken 如果认证失败则在认证过程中抛出异常
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        request.setCharacterEncoding("UTF8");
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        String bodyStr = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        String username;
        String password;
        try {
            JSONObject bodyJson = JSON.parseObject(bodyStr);
            username = bodyJson.getString("username");
            password = bodyJson.getString("password");
            if (StringUtils.isBlank(username)) {
                throw new BadCredentialsException("用户名未上传");
            }
            if (StringUtils.isBlank(password)) {
                throw new BadCredentialsException("密码未上传");
            }
        } catch (Exception e) {
            log.error("请求参数格式不合法", e);
            throw new BadCredentialsException("请求参数格式不合法");
        }
        MyUsernamePasswordAuthenticationToken authRequest = MyUsernamePasswordAuthenticationToken.MyUsernamePasswordAuthenticationTokenUnAuthenticated(username, password);
        return this.getAuthenticationManager().authenticate(authRequest);
    }


}