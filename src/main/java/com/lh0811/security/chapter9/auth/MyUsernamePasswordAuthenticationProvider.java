package com.lh0811.security.chapter9.auth;

import com.lh0811.security.chapter9.auth.token.TokenManager;
import com.lh0811.security.chapter9.modal.MyUser;
import com.lh0811.security.chapter9.service.MyUserDetailsService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

public class MyUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    private MyUserDetailsService myUserDetailsService;

    private PasswordEncoder passwordEncoder;

    private TokenManager tokenManager;


    // 构造方法
    public MyUsernamePasswordAuthenticationProvider(MyUserDetailsService myUserDetailsService, PasswordEncoder passwordEncoder, TokenManager tokenManager) {
        this.myUserDetailsService = myUserDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.tokenManager = tokenManager;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 获取未认证的token
        MyUsernamePasswordAuthenticationToken authenticationToken = (MyUsernamePasswordAuthenticationToken) authentication;
        String username = authenticationToken.getPrincipal();// 获取凭证也就是用户的手机号
        String password = authenticationToken.getCredentials(); // 获取输入的验证码
        // 获取用户信息
        MyUser myUser = (MyUser) myUserDetailsService.loadUserByUsername(username);
        // 验证密码是否匹配
        if (!passwordEncoder.matches(password, myUser.getUsers().getPassword())) {
            throw new BadCredentialsException("用户名或密码错误");
        }
        // 获取已认证的token
        MyUsernamePasswordAuthenticationToken authenticated = MyUsernamePasswordAuthenticationToken.MyUsernamePasswordAuthenticationTokenAuthenticated(myUser);
        try {
            String token = tokenManager.generateToken(authenticated);
            ((MyUser) authenticated.getDetails()).setToken(token);
            return authenticated;
        } catch (Exception e) {
            throw new BadCredentialsException("Token口令生成失败");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MyUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
