package com.lh0811.security.chapter9.auth;

import com.lh0811.security.chapter9.modal.MyUser;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class MyUsernamePasswordAuthenticationToken extends AbstractAuthenticationToken {

    private String username;
    private String password;

    private MyUsernamePasswordAuthenticationToken(Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
    }

    // 未认证时
    public static MyUsernamePasswordAuthenticationToken MyUsernamePasswordAuthenticationTokenUnAuthenticated(String username, String password) {
        MyUsernamePasswordAuthenticationToken token = new MyUsernamePasswordAuthenticationToken(null);
        token.username = username;
        token.password = password;
        token.setAuthenticated(Boolean.FALSE);
        return token;
    }

    // 认证成功后
    public static MyUsernamePasswordAuthenticationToken MyUsernamePasswordAuthenticationTokenAuthenticated(MyUser myUser) {
        MyUsernamePasswordAuthenticationToken authenticationToken = new MyUsernamePasswordAuthenticationToken(myUser.getAuthorities());
        // 认证成功后，将密码清除
        myUser.getUsers().setPassword(null);
        // 设置为已认证状态
        authenticationToken.setAuthenticated(Boolean.TRUE);
        // 用户详情为MyUser
        authenticationToken.setDetails(myUser);
        return authenticationToken;
    }

    @Override
    public String getCredentials() {
        return this.password;
    }

    @Override
    public String getPrincipal() {
        return this.username;
    }


}