package com.lh0811.security.chapter9.auth.handler;

import com.alibaba.fastjson.JSON;
import com.lh0811.security.chapter9.common.ServerResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;

public class JsonAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        ServerResponse<String> serverResponse = ServerResponse.createByError(exception.getMessage());
        response.getWriter().write(JSON.toJSONString(serverResponse));
    }
}
