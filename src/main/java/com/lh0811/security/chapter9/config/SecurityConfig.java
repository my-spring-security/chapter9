package com.lh0811.security.chapter9.config;

import com.lh0811.security.chapter9.auth.MyUsernamePasswordAuthenticationProvider;
import com.lh0811.security.chapter9.auth.MyUsernamePasswordLoginConfig;
import com.lh0811.security.chapter9.auth.TokenSecurityContextHolderFilter;
import com.lh0811.security.chapter9.auth.handler.*;
import com.lh0811.security.chapter9.auth.token.TokenManager;
import com.lh0811.security.chapter9.service.MyUserDetailsService;
import jakarta.annotation.Resource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.SecurityContextHolderFilter;

@Configuration
@EnableMethodSecurity
public class SecurityConfig {

    @Resource
    private MyUserDetailsService myUserDetailsService;

    // token 管理器
    @Resource
    private TokenManager tokenManager;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    static GrantedAuthorityDefaults grantedAuthorityDefaults() {
        return new GrantedAuthorityDefaults("MYROLE_");
    }

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // 所有请求都需要认证后访问
        http.authorizeHttpRequests((authorizeHttpRequests) -> authorizeHttpRequests.anyRequest().authenticated());
        // 禁用默认的formLogin
        http.formLogin((formLogin) -> formLogin.disable());
        // 禁用httpBasic
        http.httpBasic((httpBasic) -> httpBasic.disable());
        // 禁用csrf
        http.csrf((csrf) -> csrf.disable());
        // 将token解析认证信息的Filter 添加到SecurityContextHolderFilter之后
        http.addFilterAfter(new TokenSecurityContextHolderFilter(tokenManager), SecurityContextHolderFilter.class);
        // 增加自定义的AuthenticationProvider
        http.authenticationProvider(new MyUsernamePasswordAuthenticationProvider(myUserDetailsService,passwordEncoder(),tokenManager));
        // 启用自定义认证流程
        http.apply(new MyUsernamePasswordLoginConfig())
                .loginPage("/my-login.html")
                .loginProcessingUrl("/my_custom/login")
                .loginSuccessHandler(new JsonAuthenticationSuccessHandler())
                .loginFailHandler(new JsonAuthenticationFailureHandler());
        // 注销
        http.logout((logout) -> logout
                .logoutUrl("/my_custom/logout")
                .logoutSuccessHandler(new JsonLogoutSuccessHandler(tokenManager)));
        // 异常
        http.exceptionHandling((exception)->exception
                // 用户未认证异常处理。 未认证的情况下默认是LoginUrlAuthenticationEntryPoint 跳转到 请求用户认证的页面
                .authenticationEntryPoint(new JsonAuthenticationEntryPoint())
                // 鉴权失败异常处理。
                .accessDeniedHandler(new JsonAccessDeniedHandler())
        );
        return http.build();
    }

}
