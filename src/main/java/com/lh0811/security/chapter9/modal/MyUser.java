package com.lh0811.security.chapter9.modal;

import com.lh0811.security.chapter9.repository.entity.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MyUser implements UserDetails {

    private Users users;

    // 用户认证成功后颁发的token
    private String token;

    private List<GrantedAuthority> authorities = new ArrayList<>();


    public static MyUser createByUsers(Users users) {
        MyUser myUser = new MyUser();
        myUser.users = users;
        return myUser;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return users.getUsername();
    }

    @Override
    public String getUsername() {
        return users.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.users.getEnabled();
    }

}