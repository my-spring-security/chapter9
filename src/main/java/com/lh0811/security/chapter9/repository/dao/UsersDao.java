package com.lh0811.security.chapter9.repository.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lh0811.security.chapter9.repository.entity.Users;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-07-24 17:43:19
 */
public interface UsersDao extends IService<Users> {

}
