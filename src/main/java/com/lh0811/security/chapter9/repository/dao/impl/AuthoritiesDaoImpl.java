package com.lh0811.security.chapter9.repository.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh0811.security.chapter9.repository.dao.AuthoritiesDao;
import com.lh0811.security.chapter9.repository.dao.mapper.AuthoritiesMapper;
import com.lh0811.security.chapter9.repository.entity.Authorities;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-07-24 17:43:19
 */
@Service
public class AuthoritiesDaoImpl extends ServiceImpl<AuthoritiesMapper, Authorities> implements AuthoritiesDao {

}
