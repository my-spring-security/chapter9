package com.lh0811.security.chapter9.repository.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh0811.security.chapter9.repository.dao.UsersDao;
import com.lh0811.security.chapter9.repository.dao.mapper.UsersMapper;
import com.lh0811.security.chapter9.repository.entity.Users;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-07-24 17:43:19
 */
@Service
public class UsersDaoImpl extends ServiceImpl<UsersMapper, Users> implements UsersDao {

}
