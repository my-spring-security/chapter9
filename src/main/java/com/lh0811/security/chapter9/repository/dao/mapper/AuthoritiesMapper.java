package com.lh0811.security.chapter9.repository.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh0811.security.chapter9.repository.entity.Authorities;

/**
 * 
 *
 * @author 
 * @email 
 * @date 2023-07-24 17:43:19
 */
public interface AuthoritiesMapper extends BaseMapper<Authorities> {

}

