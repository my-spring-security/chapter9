package com.lh0811.security.chapter9.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 
 * 
 * @author 
 * @email 
 * @date 2023-07-24 17:43:19
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@TableName("users")
public class Users implements Serializable {

	@TableId(type= IdType.NONE)
	private String username;
	private String password;
	private Boolean enabled;

}
